
var debug = window.location.href;

if(debug.indexOf('.local') > 0) {
	// watch size of window

	$('<div class="sizeOfWindow" style="position:fixed; bottom: 50px; right: 50px; background: yellow; color: black; z-index: 9999;"/>').appendTo($('body'));

	$(window).resize(function(){
		var height = $(this).height(),
			width = $(this).width();
		$('.sizeOfWindow').html(width + 'x' + height);
	}).trigger('resize');
}


$(window).resize(function(){
	var width = $(this).width(),
		height = $(this).height(),
		ratio = width / height,
		goodRatio = 1280 / 800,
		img = $('.big-bg img'),
		minHeight = null,
		minWidth = null;

	if(width < 768) {
		//return;
	}

	if(goodRatio > ratio) {
		minWidth = 'auto';
		minHeight = '100%';
	} else {
		
		minWidth = '100%';
		minHeight = 'auto';
	}

	img.css({
		height : minHeight,
		width: minWidth
	});

}).trigger('resize');
